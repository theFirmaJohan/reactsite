import React, { Component } from 'react';
import Footer from "./components/Footer";
import { BrowserRouter, Route } from 'react-router-dom';
import Home from './components/Home';
import Sovellus from './components/Sovellus';
import Pesu from './components/Pesu';
import Myynti from './components/Myynti';
import Navbar from './components/Navbar';


class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Navbar/>
          <Route exact path='/' component={Home} />
          <Route path='/Sovellus' component={Sovellus} />
          <Route path='/Pesu' component={Pesu} />
          <Route path='/Myynti' component={Myynti} />
          <Footer />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
