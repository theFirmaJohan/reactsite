import React, { Component } from "react";
import "./css/Footer.css";

class Footer extends Component {
    render() {
        return(
            <footer class="footer">
                <div id="made">
                    © 2019<br />
                </div>
                {/*<div id="some">
                        Instagram<br />
                        Twitter<br />
                        facebook
        </div>*/}
                <div id="contacts">
                    Yhteydenotot: <br />
                    **** ***** <br />
                    +358****** <br />
                    ***@outlook.fi
                </div>
            </footer>
        );
    }
}

export default Footer;