import React from "react";
import {Link} from "react-router-dom";
import "./css/Navbar.css";


const Navbar = () => {
    return(
        <nav className="nav-wrapper">
                <ul className="left">
                    <li><Link to={"/"}>Home</Link></li>
                    <li><Link to={"/sovellus"}>Sovellus</Link></li>
                    <li><Link to={"/pesu"}>Pesu</Link></li>              
                    <li><Link to={"/myynti"}>Myynti</Link></li>
                    <li className="right">Labco Oy</li>
                </ul>
        </nav>
    )
}

export default Navbar