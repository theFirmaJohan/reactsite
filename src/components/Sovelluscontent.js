import React from "react";
import "./css/Sovelluscontent.css";

const Sovelluscontent = () => {
    return(
        <div className="content">
            <h4>Puhelin sovellus</h4>
            <p>Puhelin sovelluksen mainostekstiä. </p>
            <br/>
            <br/>
            <h5>Ota yhteyttä:</h5>
            <p>*******</p>
            <p>***@outlook.fi</p>
            <p>+358******</p>
        </div>
    );
}
export default Sovelluscontent;