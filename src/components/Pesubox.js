import React from "react";
import "./css/Box.css";

const Pesubox = () => {
    return(
        <div className="pesu-box">
            <h1>Autopesu</h1>
            <p>
                ***@outlook.fi<br/>
                +358*****<br/>
            </p>
        </div>
    );
}

export default Pesubox;