import React from 'react';
import "./css/Box.css";

const Sovellusbox = () => {
    return(
        <div className="sovellus-box">
            <h1>Sovellus</h1>
            <p>
                ***@outlook.fi<br/>
                +358*****<br/>
            </p>
        </div>
    );
}

export default Sovellusbox;