import React from "react";
import Sovelluscontent from "./Sovelluscontent.js";
import "./css/Container.css";

const Sovellus = () => {
    return(
        <div className="container">
            <img id="sovellusKuva" src="#" alt="Kuva applikaatiosta"></img>
            <Sovelluscontent />
        </div>
    )
}

export default Sovellus