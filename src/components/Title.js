import React from "react";
import "./css/Title.css";

const Title = () => {
    return(
        <div>
            <h1 className="title-container">
                ReactSite
            </h1>
        </div>
    )
}

export default Title;