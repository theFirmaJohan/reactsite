import React from "react";
import "./css/Container.css";
import Sovellusbox from "./Sovellusbox";
import Myyntibox from "./Myyntibox";
import Pesubox from "./Pesubox";
import Title from "./Title";

const Home = () => {
    return(
        <div className="container">
            <Title />
            <Sovellusbox />
            <Myyntibox />
            <Pesubox />
        </div>
    )
}

export default Home